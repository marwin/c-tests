#include"Datastructures/Dictionary.h"
#include<stdio.h>
#include"Datastructures/String.h"



int main ()
{
	// Use Dicts with Integer
	Dictionary<int,int> d ;
	d.AddOrUpdate( 2, 3 );
	d.AddOrUpdate( 3, 3 );
	d.AddOrUpdate( 3, 5 );
	d.Del( 2 ) ;
	d.AddOrUpdate( 2, 3 );
	int x = d.Get( 2 ) ;

	// Use Strings
	printf("test Dictionary 3 = %d \n" , x );
	printf("test Dictionary 5 = %d \n" , d.Get(3) );
	String Str ;
	Str =  ( char* ) "test String" ;
	printf("%s\n" , Str.ToString() ) ;
	
	// Use Stings in Dict
	String s = (char*) "lala01" ;
	Dictionary<int,String> d2 ;
	d2.AddOrUpdate( 2 , s ) ;
	String g ;
	printf("%s\n" , d2.Get(2).ToString()) ;

};
