#ifndef __MATRIX__

	#define __MATRIX__

template< class Type >	
class Matrix 
{

	private :
		unsigned int cols ;
		unsigned int rows ;
		Type **hook ;
	public :
		Matrix ( int Cols, int Rows ) ;
		~Matrix() ;
		
		void Truncate();

};
	
#endif 

