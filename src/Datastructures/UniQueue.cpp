#include"UniQueue.h"


template<class Type>
UniQueue<Type>::UniQueue(){
  head = 0 ;
  tail = 0 ;
}

template<class Type>
UniQueue<Type>::~UniQueue(){
	Truncate() ;
}

template<class Type>
void UniQueue<Type>::Enqueue ( Type v ){
  node* tmp = new node ;
  tmp->val =  v ;
  tmp->last = tail ;
  tail = tmp ; 
}

template<class Type>
Type UniQueue<Type>::Dequeue ( ) {
	Type x ;
	if ( IsEmpty() ) return x ;
	node* tmp = head ;
	head = head->next ;
	x = tmp->val ;
	delete tmp ;
	return x ;
}

template<class Type>
bool UniQueue<Type>::IsEmpty(){
  return ( head == 0 ) ;
}

template<class Type>
unsigned int UniQueue<Type>::Count(){
  int count = 0 ;
  node* tmp = this->head ;
  while ( tmp != 0 ){
    tmp = tmp->next ;
    count++ ;
  }
  return count ;
}

template<class Type>
void UniQueue<Type>::Truncate() 
{
  node* tmp ;
  while ( head != 0 ) {
    tmp = head ;
    head = head->next ;
    delete tmp ;
  }
  head = tail = 0 ;
}


template<class Type>
UniQueue<Type> UniQueue<Type>::operator= ( UniQueue<Type> x )
{
	if ( &x == this )
		return *this ;

	Truncate() ;

	UniQueue<Type> newQ ;
	node* tmp = x.head ;
	while ( tmp != 0 )
	{
		newQ.Enqueue ( tmp->val );
		Enqueue ( tmp->val );
		tmp = tmp->next ;
	}
	return newQ ;
}



template class UniQueue<int>;
template class UniQueue<long>;
template class UniQueue<unsigned int>;
template class UniQueue<float>;
template class UniQueue<double>;
template class UniQueue<char>;
template class UniQueue<String>;



