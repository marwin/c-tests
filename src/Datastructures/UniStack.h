#include"String.h"

template<class Type>
class UniStack {


  protected :
    struct node{
      Type val ;
      node* pred ;
      node(){
        pred = 0 ;
      }
    };
    node* head ;

  public : 
    UniStack() ;
    ~UniStack();
    bool IsEmpty();
    Type Pop ( );
    void Push ( Type ) ;
    unsigned int Count() ;
    void Truncate () ;
    //UniStack<Type> operator= ( UniStack<Type> x );
};

