#include"Matrix.h"


template< class Type >
Matrix<Type>::
Matrix(int Cols, int Rows )
{
	hook = 0 ;
	cols = Cols ;
	rows = Rows ;
	hook = new Type*[ cols ] ;
	for ( int i=0 ; i < cols ; i++ )
		hook[i] = new Type [ rows ] ;
}

template< class Type >
Matrix<Type>::
~Matrix()
{
	//Truncate () ;
	for ( int i = 0 ; i < cols ; i++ )
		delete [] hook[i] ;
	delete [] hook ;
}


template< class Type >
void
Matrix<Type>::
Truncate()
{
	int i,j ;
	Type Null ;
	for ( i=0 ; i < cols ; i++ )
		for ( j = 0 ; j < rows ; j++ )
			hook[i][j] = Null ;
}
