#include"String.h"
template<class Type>
class UniQueue {

  protected :
    struct node {
      Type val ;
      node* next ;
      node* last ;
      node(){
        next = last = 0 ;
      }
    };
    node* head ;
    node* tail ;
    
    
  public :
    UniQueue();
    ~UniQueue();
    void Enqueue ( Type v ) ;
    Type Dequeue ( ) ;
    bool IsEmpty() ;
    unsigned int Count();
    void Truncate() ;
    UniQueue<Type> operator= ( UniQueue<Type> x );

};
