#ifndef __DICTIONARY__

	#define __DICTIONARY__
	#include"UniQueue.h"
	#include"String.h"

template< class IndexType, class ValueType >
class Dictionary
{
	private :
		struct node
		{
			ValueType value ;
			IndexType key ;

			node  *next ;
			node  *pred ;

			node( IndexType Key, ValueType Value)
			{
				key = Key ;
				value = Value ;
				next = 0 ;
				pred = 0 ;
			}
			node()
			{
				next = 0 ;
				pred = 0 ;
			}
		};
		node *hook ;
		node *searchHook ;

	public :
		Dictionary();
		~Dictionary() ;
		bool IsEmpty() ;
		void Truncate () ;
		bool IsKey( IndexType Key );
		ValueType Get( IndexType Key );
		void AddOrUpdate( IndexType Key, ValueType Value );
		void Del ( IndexType Key ) ;
		unsigned int Count( ) ;
		UniQueue< IndexType > GetKeys() ;
		Dictionary< IndexType , ValueType > operator= ( Dictionary< IndexType , ValueType > other ) ;
};


#endif



