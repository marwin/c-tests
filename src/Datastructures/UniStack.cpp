#include"UniStack.h"


template<class Type>
UniStack<Type>::UniStack() {
  this->head = 0 ;
}


template<class Type>
UniStack<Type>::~UniStack(){
	Truncate() ;
}


template<class Type>
bool UniStack<Type>::IsEmpty(){
  return ( head==0 );
}


template<class Type>
Type UniStack<Type>::Pop ( ){
	Type i ;
	if ( this->IsEmpty() ) return i ;
	node* tmp= this->head ;
	this->head = this->head->pred ;
	i = tmp->val ;
	delete tmp ;
	return i ;
}


template<class Type>
void UniStack<Type>::Push ( Type x ){
  node* tmp ;
  tmp = new node ;
  tmp->pred = this->head ;
  tmp->val = x ;
  this->head = tmp ; 
}


template<class Type>
unsigned int UniStack<Type>::Count()
{
	node* tmp = head ;
	int count = 0 ;
	while ( tmp != 0 )
	{
		count++ ;
		tmp = tmp->pred ;
	} 
	return count ;
}


template<class Type>
void UniStack<Type>::Truncate () 
{
  node* tmp ;
  while ( this->head != 0 ){
    tmp = this->head ;
    this->head = this->head->pred ;
    delete tmp ;
  }
  head = 0 ;
}

/* 
template<class Type>
UniStack<Type> UniStack<Type>::operator= ( UniStack<Type> x )
{
	if ( &x == this )
		return *this ;
	UniStack<Type> newS ;
	node* tmp = x.head ;
	while ( tmp != 0 )
	{
		// add nodes reverse ? 
		newS.Push( 
		tmp = tmp->pred ;
	}
	

}
*/

template class UniStack<int>;
template class UniStack<long>;
template class UniStack<unsigned int>;
template class UniStack<float>;
template class UniStack<double>;
template class UniStack<char>;
template class UniStack<String>;


