#ifndef __String__

#define __String__

#ifndef __String_Array_Size__
  #define __String_Array_Size__ 255
#endif 

class String
{
  private :
    char str[ __String_Array_Size__ ] ;
    void set( char[] );
    char* outString  ;

  public :
   String();
   String( String *newString );
   String( char newString[] );
   ~String() ;
   String operator=( char newString[] );
   String operator=( String newString );
   char &operator[]( int Index );
   bool operator==( String  otherString ) ;
   String operator+=( char otherString[] );
   String operator+=( String  otherString );
   //char operator[]( unsigned int, char );
   unsigned int Length() ;
   void GetAsCharacter( char result[] );
   bool IsEmpty() ;
   
   char* ToString() ;
   
   bool operator>= ( String otherString );
   bool operator> ( String otherString );
   bool operator<= ( String otherString ) ;
   bool operator< ( String otherString ) ;
};
#endif 
