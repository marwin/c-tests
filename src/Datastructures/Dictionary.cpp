#ifndef __DICTIONARY__
	#include"Dictionary.h"
#endif



template< class IndexType, class ValueType >
Dictionary< IndexType, ValueType >::
Dictionary()
{
	hook = 0 ;
	searchHook = 0 ;
}


template< class IndexType, class ValueType >
Dictionary< IndexType, ValueType >::
~Dictionary()
{
	Truncate() ;
}


template< class IndexType, class ValueType >
bool
Dictionary< IndexType, ValueType >::
IsEmpty ()
{
	return ( hook == 0 );
}


template< class IndexType, class ValueType >
void
Dictionary< IndexType, ValueType >::
Truncate ()
{
	if ( IsEmpty() )
		return ;

	node* tmp ;
	while ( hook != 0 )
	{
		tmp = hook->next ;
		delete hook ;
		hook = tmp ;
	}
	searchHook = 0 ;
}


template< class IndexType, class ValueType >
bool Dictionary< IndexType, ValueType >::
IsKey( IndexType Key )
{
	if ( IsEmpty() )
		return false ;

	if ( searchHook && searchHook->key == Key )
		return true ;

	node *tmp = hook ;
	while ( tmp != 0 )
	{
		if ( tmp->key == Key )
		{
			searchHook = tmp ;
			return true ;
		}
		tmp = tmp->next ;
	}
	searchHook = 0 ;
	return false ;
}


template< class IndexType, class ValueType >
ValueType Dictionary< IndexType, ValueType >::
Get( IndexType Key )
{
	ValueType result ;
	bool isItem = IsKey( Key );
	if ( isItem )
	{
		result =  searchHook->value ;
	}
	return result ;
}


template< class IndexType, class ValueType >
void Dictionary< IndexType, ValueType >::
AddOrUpdate( IndexType Key, ValueType Value )
{
	if ( IsKey( Key ) )
	{
		searchHook->value = Value ;
	}
	else
	{
		searchHook = hook  ;
		node* tmp = new node ;

		tmp->next = hook ;
		if ( hook != 0 )
			hook->pred = tmp ;
		// tmp->pred = 0 ;

		tmp->value = Value ;
		tmp->key = Key ;

		hook = tmp ;
	}
}


template< class IndexType, class ValueType >
void
Dictionary< IndexType, ValueType >::
Del ( IndexType Key )
{
	if ( IsKey ( Key ))
	{
		node* tmp = searchHook ;

		if ( tmp == hook )
		{
			hook = hook->next ;
			if ( hook )
				hook->pred = 0 ;
			delete tmp ;
			searchHook = hook ;
		}
		else
		{
			if ( tmp->pred )
				tmp->pred->next = tmp->next ;
			if ( tmp->next ) 
				tmp->next->pred = tmp->pred ;
			searchHook = hook ;
			delete tmp ;
		}
	}

}


template< class IndexType, class ValueType >
unsigned int
Dictionary< IndexType, ValueType >::
Count( )
{
	if ( IsEmpty() )
		return 0 ;
	node* tmp ;
	int count = 0 ;
	while ( tmp != 0 )
	{
		count ++ ;
		tmp = tmp->next ;
	}
	return count ;
}


template< class IndexType, class ValueType >
UniQueue< IndexType > 
Dictionary< IndexType, ValueType >::
GetKeys() 
{
	UniQueue< IndexType > Q ;
	node* tmp = hook ;
	while ( tmp != 0 )
	{
		Q.Enqueue( tmp->key );
		tmp = tmp->next;
	}
	return Q ;
}




template< class IndexType, class ValueType >
Dictionary< IndexType , ValueType >
Dictionary< IndexType, ValueType >::
operator= ( Dictionary other ) 
{
	if ( &other == this ) 
		return *this;
	
	UniQueue< IndexType > keys  ;
	keys = other.GetKeys() ;
	
	Truncate () ;
	
	Dictionary< IndexType, ValueType > newDict ; 
	while ( ! keys.IsEmpty() )
	{
		IndexType i = keys.Dequeue () ;
		newDict.AddOrUpdate ( i , other.Get(i) );
		AddOrUpdate( i , other.Get(i) ) ;
	}
	return newDict ;
}



template class Dictionary< int, int > ;
template class Dictionary< unsigned int, unsigned int > ;

template class Dictionary< int, String > ;




