#include"String.h"
#include<string.h>

String::String()
{
  set( (char*) "" );
  outString = 0 ;
}


String::String( char string[] )
{
  if ( string == NULL )
    set( (char*)"");
  else
    set( string ) ;
  outString = 0 ;
}


String::String( String *newString )
{
  char tmp[ __String_Array_Size__ ] ;
  newString->GetAsCharacter( tmp );
  set( tmp );
  outString = 0 ;
}


String::~String()
{

}

void
String::set( char newString[] )
{
  strcpy( str , newString ) ;
  str[ __String_Array_Size__ -1 ] = 0 ;
}

String
String::operator=( char newString[] )
{
  set( newString );
  String ret( newString );
  return ret;
}


String
String::operator=( String newString )
{
  //char* tmp = new char[ newString.Length()+1 ];
  char tmp [ newString.Length()+1 ];
  newString.GetAsCharacter( tmp ) ;

  set(tmp);

  String ret = tmp ;
  //delete [] tmp ;

  return ret ;

}


char
&String::operator[]( int Index )
{
  if ( Index < 0 )
    return str[ Length() + Index ];
  else
    return str[ Index ];
}

/*
char
String::&operator[]( unsigned int Index, char Char )
{
  //if ( Index
  str[ Index ] = Char ;
  return str[ Index ];
}
*/

bool
String::operator==( String otherString )
{
  //char *other = new char[ otherString.Length()+1 ];
  char other [ otherString.Length()+1 ] ;
  otherString.GetAsCharacter( other ) ;
  bool ret = (strcmp( str, other ) == 0 );
  //delete [] other ;
  return ret ;
}

String
String::operator+=( char otherString[] )
{
  //char* tmp = new char[ Length() + strlen(otherString) + 1];
  char tmp[ Length() + strlen(otherString) +1 ] ;

  strcpy( tmp , str );
  strcat( tmp , otherString );

  set ( tmp );

  //delete [] tmp ;

  String ret = str;
  return ret ;
}

String
String::operator+=( String otherString )
{
  //char* tmp = new char[ otherString.Length()+1 ];
  char tmp [ otherString.Length()+1 ];
  otherString.GetAsCharacter( tmp );

  this->operator+=( tmp );
  //delete [] tmp;

  String ret = str;

  return ret ;
}


unsigned int
String::Length()
{
  return strlen( str );
}


void
String::GetAsCharacter( char result[] )
{
  strcpy( result , str );
}

bool
String::IsEmpty()
{
  return Length() == 0 ;
}

char* 
String::ToString() 
{
	if ( outString )
	{
		delete outString ;
	}
	outString = new char [ strlen( str ) +1 ] ;
	strcpy ( outString , str ) ;
	return outString ;
}



bool
String::operator>= ( String otherString )
{
	if ( this->Length() > otherString.Length() )
		return true ;

	if ( this->Length() < otherString.Length() )
		return false ;


	if ( true == operator==(otherString) )
		return true ;

	unsigned int len = Length() ;
	//char *other = new char[ len+1 ] ;
	char other [ len +1 ];

	otherString.GetAsCharacter( other ) ;

	for ( int i=0; i < len; i++ )
	{
		if ( str[ i ] < other[ i ] )
		{
			//delete[] other ;
			return false ;
		}
		else if ( str[i] > other[i] )
		{
			return true ;
		}
	}
	//delete[] other ;
	return true ;
}


bool
String::operator> ( String otherString )
{
	return (  (!operator==(otherString)) && operator>=(otherString) ) ;
}


bool
String::operator<= ( String otherString )
{
	return (  operator==(otherString) || (!operator>=(otherString)) ) ;
}


bool
String::operator< ( String otherString )
{
	return (  !operator>=(otherString) ) ;
}
